import logo from './logo.svg';
import './App.css';
import React, {useState, useEffect} from 'react';



function App() { 

  const [type, setType] = useState('users')

  useEffect(() => {
    console.log('type change');
  }, [type])


  return (
    <div className="App">
      <h1>ресурс: {type}</h1>

      <button onClick={() => setType('users')}>пользователи</button>
      <button onClick={() => setType('todo')}>Todo</button>
      <button onClick={() => setType('posts')}>посты</button>
    </div>
  );
}

export default App;
